_model: page
---
title: Politique de protection des données et vie privée
---
body:

<!-- ne pas modifier ici, faire des propositions de modifs sur le pad : https://pad.exegetes.eu.org/p/g.9PizVXtKx3X5Vpe3$Politique%20vie%20privee%20exegetes -->

Cette page a pour objet de vous informer sur la collecte, l'utilisation, la conservation et la communication de vos données faites dans le cadre de votre navigation sur ce site Web et des listes de diffusion décrites ci-dessous et, plus globalement, dans le cadre des activités des Exégètes Amateurs. Les Exégètes Amateurs sont un groupe de travail associatif (en quelque sorte, une association de fait).

## Site web

Votre navigation sur ce site ne donne lieu à la collecte d'aucune donnée à caractère personnel vous concernant qui soit sous la responsabilité des Exégètes. Toutefois, la collecte de certaines données peut être effectuée par des tiers (dont FDN, comme décrit ci-après) à l'occasion de votre navigation.

Le présent site web est hébergé par [l'association FDN](https://fdn.fr/mentionslegales/), qui traite des données à caractère personnel à l'occasion de la visite de sites qu'elle héberge, aux fins d'assurer la sécurité de ses applications, machines et infrastructures et de déboguer en cas de défaillance ou de fonctionnement mauvais ou inefficient; dans l'intérêt légitime de l'association d'avoir des infrastructures en bon état et d'assurer un service efficace. À ce titre, **FDN conserve pendant 1 semaine** les «logs» de serveur web pour ce site (avec rotation quotidienne des fichiers). Ces logs suivent les bonnes pratiques et contiennent des données personnelles: **votre adresse IP, votre date et heure de visite, ainsi que des informations techniques sur chaque requête et le logiciel utilisé pour la visite**. Ces données sont accessibles aux administrateurs du serveur web et ne sont communiquées volontairement à aucun tiers. FDN prend l'ensemble des mesures techniques et organisationnelles nécessaires à la sécurité de ses infrastructures et, par conséquent, à la protection de vos données, au regard de son modèle de menace et des risques inhérents à tout site web public. 

## Listes de diffusion <span id="liste-media">médias</span> et <span id="liste-infos">infos</span> et autres moyens de communication

Les Exégètes Amateurs peuvent, avec votre consentement, vous envoyer par email (ou par d'autres moyens de communication), des communications relatives aux Exégètes Amateurs, concernant les contentieux qu'ils engagent et/ou les associations dont ils émanent. Vous pouvez retirer votre consentement à tout moment, notamment en suivant les liens inclus en bas de chaque communication par email. Votre adresse email (ou tout autre identifiant) n'est pas conservée au-delà de la durée de votre inscription à chaque liste. Votre adresse email (ou autre)n'est accessible qu'aux administrateurs de la liste.

## Vos droits et nos obligations

Vous pouvez exercer à tout moment vos droits d'accès, de rectification, de limitation et d'effacement de vos données et, pour motifs légitimes, vous opposer au traitement de vos données, en nous contactant. Vous avez le droit d'introduire une réclamation auprès d'une autorité de contrôle (p. ex. la CNIL). Vous disposez également du droit de définir des instructions sur l'accès et le traitement de vos données après votre décès conformément à la loi n<sup>o</sup> 78-17 du 6 janvier 1978. 

Rien dans cette page n'est de nature à limiter ou exclure l'obligation pour les Exégètes Amateurs (ou pour FDN ou toute autre personne) de se conformer à une demande légale des autorités compétentes concernant les traitements décrits ci-dessus. À ce jour, les Exégètes (et FDN) n'ont reçu **aucune**  demande de ce type en lien avec le présent site ou les listes de diffusion susmentionnées.

Les traitements de données à caractère personnel décrits dans la présente politique ont été notifiés à la CNIL.

## Modifications ultérieures de cette politique

Cette page concernant la protection des données et la vie privée peut être mise à jour à tout moment en prévision d'un changement de pratique en la matière. Pour tout changement relatif à un traitement fait avec votre consentement, vous serez informés préalablement par email au moins quinze jours à l'avance.

-------------------

*[v.1.1 - Dernière mise à jour : le 19 septembre 2017 [pdf]](2017-12-19-mentions-legales_vie-privee.pdf)* 

**Archives**

[v1 du 25 août](2017-08-25-mentions-legales_vie-privee.pdf)

L'ensemble des modifications, y compris de forme, sont disponibles dans le [système de contrôle de versions](https://code.ffdn.org/exegetes/site/commits/master/content/mentions-legales/vie-privee/contents.lr).
