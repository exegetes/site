_model: page
---
title: Les données de connexion
---
body:

Les données de connexion sont des données techniques générées par des fournisseurs de services de communications à l'occasion de l'utilisation de leur service par leurs utilisateurs. On parle souvent de «métadonnées» car ce sont des données à propos d'autres données ([Wikipédia](https://fr.wikipedia.org/wiki/M%C3%A9tadonn%C3%A9e)). 

En droit français, la notion de «données de connexion» recouvre :

  - les données techniques permettant l'acheminement des communications électroniques par les opérateurs (téléphonie, internet, fixe ou mobile, etc.);
  - les données permettant l'identification d'un utilisateur d'un accès à internet fourni par un fournisseur d'accès internet (FAI);
  - les données permettant l'identification d'un utilisateur de service de communication au public en ligne (“hébergeurs”).

Le droit français impose aux opérateurs, fournisseurs d'accès et “hébergeurs” (au sens de la loi no 2004-575 dite «LCEN», art. 6,I,2°) de collecter et/ou conserver certaines données de connexion pendant un an (articles L. 34-1, III, IV, V et VI; R. 10-13 du code des postes et des communications électroniques; article 6, II LCEN et décret no 2011-219). Le droit français permet également à certains services de l'administration d'accéder aux données de connexion des opérateurs, fournisseurs d'accès et hébergeurs.

### Quel est l'enjeu de ces données?

Les données de connexion sont des données techniques, elles ne sont pas pour autant dépourvues d'intérêt. Au contraire, ces données révèlent des informations très précises sur la vie privée des personnes. Nous avons sélectionné ci-dessous quelques liens, extraits ou jurisprudences à ce sujet.

Voir notre tribune à ce sujet : [Tous suspects](/posts/donnees-sur-le-net-tous-suspects/)

[Plaidoirie au Conseil constitutionnel insistant sur l'importance des données de connexion](2017-07-04-plaidoirieconseilconstitamf_001hrd.pdf)

[Travaux de recherche à ce sujet sélectionnés par l'agence Étatique de protection de la vie privée du Canada](https://www.priv.gc.ca/fr/mesures-et-decisions-prises-par-le-commissariat/recherche/consulter-les-travaux-de-recherche-sur-la-protection-de-la-vie-privee/2014/md_201410/)

-------

> De la même manière, pour une participante E, savoir qu’elle a émis un
appel très tôt le matin à sa sœur, croisé aux appels répétés passés
quelques jours plus tard à un service de planning familial à proximité,
révèle des éléments très précis sur la vie privée de cette personne et 
au
delà, permet de déduire l’objet et le contenu de ses correspondances –
avec un degré de certitude élevé et que l’agrégation de davantage de
données de connexion ne fait qu’affiner (Voir “Evaluating the privacy
properties of telephone metadata”, J. Mayera, P. Mutchlera, and J.C.
Mitchella, Edited by Cynthia Dwork, Microsoft Research Silicon
Valley, Mountain View, CA, and approved March 1, 2016, p.5 ,
disponible à l’adresse :
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4878528/)



--------------


https://www.aclu.org/files/pdfs/natsec/clapper/2013.08.26%20ACLU%20PI%20Brief%20-%20Declaration%20-%20Felten.pdf

> The privacy impact of collecting all communications metadata about a 
single person for long periods of time is qualitatively different than 
doing so over a period of days. Similarly, the privacy impact of 
assembling the call records of every American is vastly greater than the 
impact of collecting data about a single person or even groups of 
people. Mass collection not only allows the government to learn 
information about more people, but it also enables the government to 
learn new, previously private facts that it could not have learned 
simply by collecting the information about a few, specific 
individuals.(p.22)


---------------


<https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4878528/>

> "One of themost controversial principles, both in the United States and 
abroad, is that communications metadata receives substantially less 
protection than communications content. [...] In this paper, we attempt 
to shed light on the privacy properties of telephone metadata. Using a
crowdsourcing methodology,we demonstrate that telephone metadata is 
densely interconnected, can trivially be reidentified, and can be used 
to draw sensitive inferences." (p.1)

> "The following vignettes are reflective of the types of inferences we 
were able to draw.
[...]Participant B received a long phone call from the cardiology group 
at a regional medical center, talked briefly with a medical laboratory, 
answered several short calls from a local drugstore, and made brief 
calls to a self-reporting hotline for a cardiac arrhythmia monitoring 
device."
[...]Participant E made a lengthy phone call to her sister early one 
morning. Then, 2 days later, she called a nearby Planned Parenthood 
clinic several times. Two weeks later, she placed brief additional calls 
to Planned Parenthood, and she placed another short call 1 month after". 
(p. 5)

 > "Using public sources, we were able to confirm that participant B had a 
cardiac arrhythmia"  (p.5)

(Source : "Evaluating the privacy properties of telephone metadata", 
Jonathan Mayera,b,1, Patrick Mutchlera, and John C. Mitchella aSecurity 
Laboratory, Department of Computer Science, Stanford University, 
Stanford, CA 94305; and bStanford Law School, Stanford University, 
Stanford, CA 94305 Edited by Cynthia Dwork, Microsoft Research Silicon 
Valley, Mountain View, CA, and approved March 1, 2016 (received for 
review April 27, 2015).


-------------------

> "Telephony metadata is easy to aggregate and analyze because it is, by 
its nature,structured data [...] the time and date information 
associated with the beginning and end of each call will be stored in a 
predictable, standardized format. [...] The structured nature of 
metadata makes it easy to analyze massive datasets using sophisticated 
data-mining and link-analysis programs."  (Written Testimony of Edward 
W. Felten Professor of Computer Science and Public Affairs, Princeton 
University United States Senate, Committee on the Judiciary Hearing on 
Continued Oversight of the Foreign Intelligence Surveillance Act October 
2, 2013, p. 4).

------------------


CJUE : 

<http://curia.europa.eu/juris/documents.jsf?pro=&lgrec=en&nat=or&oqp=&lg=&dates=&language=en&jur=C%2CT%2CF&cit=none%252CC%252CCJ%252CR%252C2008E%252C%252C%252C%252C%252C%252C%252C%252C%252C%252Ctrue%252Cfalse%252Cfalse&num=C-203%252F15&td=%3BALL&pcs=Oor&avg=&page=1&mat=or&jge=&for=&cid=903104>


Point 259 des conclusions de l'AG sur l'affaire Télé 2 :

> Je tiens à souligner que les risques liés à l’accès aux données 
relatives aux communications (ou « métadonnées ») peuvent être 
équivalents, voire supérieurs à ceux résultant de l’accès au contenu de 
ces communications, comme l’ont souligné Open Rights Group et Privacy 
International, la Law Society of England and Wales ainsi qu’un récent 
rapport du Haut‑Commissariat des Nations unies aux droits de l’homme 
(86). En particulier, et comme le montrent les exemples précités, les « 
métadonnées » permettent un catalogage presque instantané d’une 
population dans son entièreté, ce que ne permet pas le contenu des 
communications.

Note de bas de page 86 des conclusions de l'AG sur Télé 2 :

> Voir, à cet égard, Conseil des droits de l’homme des Nations Unies, 
Rapport du Haut‑Commissariat des Nations Unies aux droits de l’homme sur 
le droit à la vie privée à l’ère du numérique, 30 juin 2014, 
A/HRC/27/37, n° 19 : « Dans le même ordre d’idées, d’aucuns soutiennent 
que l’interception ‐ ou la collecte ‐ de données sur une communication, 
et non plus sur le contenu de la communication, ne constitue pas à elle 
seule une immixtion dans la vie privée. Or du point de vue du droit à la 
vie privée, cette distinction n’est pas convaincante. Les agrégations 
d’informations communément appelées ‘métadonnées’ peuvent donner des 
indications sur la conduite d’un individu, ses relations sociales, ses 
préférences privées et son identité qui vont bien au‑delà de ce que l’on 
obtient en accédant au contenu d’une communication privée » (italique 
ajouté par mes soins). Voir également Assemblée générale des Nations 
Unies, Rapport du Rapporteur spécial sur la promotion et la protection 
des droits de l’homme et des libertés fondamentales dans la lutte 
antiterroriste, 23 septembre 2014, A/69/397, n° 53.

Points 98-103 de la décision Télé 2

> 98      Les données que doivent ainsi conserver les fournisseurs de 
services de communications électroniques permettent de retrouver et 
d’identifier la source d’une communication et la destination de 
celle-ci, de déterminer la date, l’heure, la durée et le type d’une 
communication, le matériel de communication des utilisateurs, ainsi que 
de localiser le matériel de communication mobile. Au nombre de ces 
données figurent, notamment, le nom et l’adresse de l’abonné ou de 
l’utilisateur inscrit, le numéro de téléphone de l’appelant et le numéro 
appelé ainsi qu’une adresse IP pour les services Internet. Ces données 
permettent, en particulier, de savoir quelle est la personne avec 
laquelle un abonné ou un utilisateur inscrit a communiqué et par quel 
moyen, tout comme de déterminer le temps de la communication ainsi que 
l’endroit à partir duquel celle-ci a eu lieu. En outre, elles permettent 
de connaître la fréquence des communications de l’abonné ou de 
l’utilisateur inscrit avec certaines personnes pendant une période 
donnée (voir, par analogie, en ce qui concerne la directive 2006/24, 
arrêt Digital Rights, point 26).

> 99      Prises dans leur ensemble, ces données sont susceptibles de 
permettre de tirer des conclusions très précises concernant la vie 
privée des personnes dont les données ont été conservées, telles que les 
habitudes de la vie quotidienne, les lieux de séjour permanents ou 
temporaires, les déplacements journaliers ou autres, les activités 
exercées, les relations sociales de ces personnes et les milieux sociaux 
fréquentés par celles-ci (voir, par analogie, en ce qui concerne la 
directive 2006/24, arrêt Digital Rights, point 27). En particulier, ces 
données fournissent les moyens d’établir, ainsi que l’a relevé M. 
l’avocat général aux points 253, 254 et 257 à 259 de ses conclusions, le 
profil des personnes concernées, information tout aussi sensible, au 
regard du droit au respect de la vie privée, que le contenu même des 
communications.

> 100    L’ingérence que comporte une telle réglementation dans les droits 
fondamentaux consacrés aux articles 7 et 8 de la Charte s’avère d’une 
vaste ampleur et doit être considérée comme particulièrement grave. La 
circonstance que la conservation des données est effectuée sans que les 
utilisateurs des services de communications électroniques en soient 
informés est susceptible de générer dans l’esprit des personnes 
concernées le sentiment que leur vie privée fait l’objet d’une 
surveillance constante (voir, par analogie, en ce qui concerne la 
directive 2006/24, arrêt Digital Rights, point 37).

> 101    Même si une telle réglementation n’autorise pas la conservation 
du contenu d’une communication et, partant, n’est pas de nature à porter 
atteinte au contenu essentiel desdits droits (voir, par analogie, en ce 
qui concerne la directive 2006/24, arrêt Digital Rights, point 39), la 
conservation des données relatives au trafic et des données de 
localisation pourrait toutefois avoir une incidence sur l’utilisation 
des moyens de communication électronique et, en conséquence, sur 
l’exercice par les utilisateurs de ces moyens de leur liberté 
d’expression, garantie à l’article 11 de la Charte (voir, par analogie, 
en ce qui concerne la directive 2006/24, arrêt Digital Rights, point 
28).

> 102    Eu égard à la gravité de l’ingérence dans les droits fondamentaux 
en cause que constitue une réglementation nationale prévoyant, aux fins 
de la lutte contre la criminalité, la conservation de données relatives 
au trafic et de données de localisation, seule la lutte contre la 
criminalité grave est susceptible de justifier une telle mesure (voir, 
par analogie, à propos de la directive 2006/24, arrêt Digital Rights, 
point 60).

> 103    En outre, si l’efficacité de la lutte contre la criminalité 
grave, notamment contre la criminalité organisée et le terrorisme, peut 
dépendre dans une large mesure de l’utilisation des techniques modernes 
d’enquête, un tel objectif d’intérêt général, pour fondamental qu’il 
soit, ne saurait à lui seul justifier qu’une réglementation nationale 
prévoyant la conservation généralisée et indifférenciée de l’ensemble 
des données relatives au trafic et des données de localisation soit 
considérée comme nécessaire aux fins de ladite lutte (voir, par 
analogie, en ce qui concerne la directive 2006/24, arrêt Digital Rights, 
point 51).


Décision Digital Rights :

> 26      À cet égard, il convient de relever que les données que doivent 
conserver les fournisseurs de services de communications électroniques 
accessibles au public ou de réseaux publics de communications, au titre 
des articles 3 et 5 de la directive 2006/24, sont, notamment, les 
données nécessaires pour retrouver et identifier la source d’une 
communication et la destination de celle-ci, pour déterminer la date, 
l’heure, la durée et le type d’une communication, le matériel de 
communication des utilisateurs, ainsi que pour localiser le matériel de 
communication mobile, données au nombre desquelles figurent, notamment, 
le nom et l’adresse de l’abonné ou de l’utilisateur inscrit, le numéro 
de téléphone de l’appelant et le numéro appelé ainsi qu’une adresse IP 
pour les services Internet. Ces données permettent, notamment, de savoir 
quelle est la personne avec laquelle un abonné ou un utilisateur inscrit 
a communiqué et par quel moyen, tout comme de déterminer le temps de la 
communication ainsi que l’endroit à partir duquel celle-ci a eu lieu. En 
outre, elles permettent de connaître la fréquence des communications de 
l’abonné ou de l’utilisateur inscrit avec certaines personnes pendant 
une période donnée.

> 27      Ces données, prises dans leur ensemble, sont susceptibles de 
permettre de tirer des conclusions très précises concernant la vie 
privée des personnes dont les données ont été conservées, telles que les 
habitudes de la vie quotidienne, les lieux de séjour permanents ou 
temporaires, les déplacements journaliers ou autres, les activités 
exercées, les relations sociales de ces personnes et les milieux sociaux 
fréquentés par celles-ci.

> 28      Dans de telles circonstances, même si la directive 2006/24 
n’autorise pas, ainsi qu’il découle de ses articles 1er, paragraphe 2, 
et 5, paragraphe 2, la conservation du contenu de la communication et 
des informations consultées en utilisant un réseau de communications 
électroniques, il n’est pas exclu que la conservation des données en 
cause puisse avoir une incidence sur l’utilisation, par les abonnés ou 
les utilisateurs inscrits, des moyens de communication visés par cette 
directive et, en conséquence, sur l’exercice par ces derniers de leur 
liberté d’expression, garantie par l’article 11 de la Charte.

> 29      La conservation des données aux fins de leur accès éventuel par 
les autorités nationales compétentes, telle que prévue par la directive 
2006/24, concerne de manière directe et spécifique la vie privée et, 
ainsi, les droits garantis par l’article 7 de la Charte. En outre, une 
telle conservation des données relève également de l’article 8 de 
celle-ci en raison du fait qu’elle constitue un traitement des données à 
caractère personnel au sens de cet article et doit, ainsi, 
nécessairement satisfaire aux exigences de protection des données 
découlant de cet article (arrêt Volker und Markus Schecke et Eifert, 
C‑92/09 et C‑93/09, EU:C:2010:662, point 47).


### Affaires liées :

- [Abrogation de la rétention généralisée des données de connexion](/dossiers/abrogationretention/index.html)
- [État d'urgence : interception en temps réel des données de connexion](/dossiers/etatdurgencetempsreel/index.html)
- [LPM : Accès administratif aux données de connexion](/dossiers/lpm/index.html)
