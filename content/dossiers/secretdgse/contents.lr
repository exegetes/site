layout:

page
collection: dossiers
title: "Système de surveillance secret de la DGSE"
tags: ["affaire:secretdgse"]
---
[L'Obs](http:

//tempsreel.nouvelobs.com/societe/20150625.OBS1569/exclusif-comment-la-france-ecoute-aussi-le-monde.html)
de juillet 2015. L'article révèle la captation massive de
communications par les services de renseignement extérieur des flux
internet en provenance ou à destination du territoire français.


D'après cet article, ce système de surveillance de la DGSE aurait été autorisé par un « décret secret ».

La **première procédure** est une procédure d'urgence: un
référé-suspension au Conseil d'État. 

Cette procédure d'urgence avait pour but de demander la suspension
immédiate du décret en attendant le jugement sur le fond.

Ce recours a été rejeté dans
[une ordonnance du 9 septembre 2015](https://exegetes.eu.org/recours/secretdgse/CEtat/2015-09-09-Ordonnance_-_De__cret_secret_393079_ocr.pdf)
signée de la main de Bernard Stirn, président de la section du
contentieux du Conseil d'État, avant même la tenue d'une audience
contradictoire. Dans cette décision, le juge des référés estime que
l'urgence n'est pas caractérisée.

La **seconde procédure** est toujours en cours devant le Conseil
d'État. Elle vise à obtenir l'annulation du décret révélé par L'Obs.

Les points attaqués portent sur les éléments suivants :

  -  Légalité externe : Décret entaché d'incompétence et adopté au terme d'une procédure irrégulière ;
  -  Légalité interne : Décret dépourvu de base légale. La loi sur le renseignement adoptée en juillet 2015 aurait du apporter un fondement légal à ce décret, mais les dispositions relatives à la surveillance internationale ont été censurées par le Conseil constitutionnel.

------------

### Surveillance secrète par la DGSE (2008-2015)

Mémoires et décisions : [Conseil d'État][secretdgseCEtat]

(En collaboration avec Spinosi & Sureau SCP)

------

#### Recours initiés par d'autres 

 -   Un avocat franco-américain [saisit la CNCIS](https://www.laquadrature.net/fr/node/9538)
 -   La French American Bar Association [saisit la CNIL](http://www.nextinpact.com/news/96635-surveillance-internationale-avocats-franco-americains-portent-plainte-aupres-cnil.htm)



[abrogationretentiondemande]: /recours/abrogationretention/demande/
[abrogationretentionCEtat]: /recours/abrogationretention/CEtat/
[amicusrenseignement]: /recours/amicusrenseignement/
[filtragecazeneuveCEtat]: /recours/filtragecazeneuve/CEtat/
[filtragecazeneuveCnil]: /recours/filtragecazeneuve/Cnil/
[filtragecazeneuveOclctic]: /recours/filtragecazeneuve/Oclctic/
[lpmCEtat]: /recours/lpm/CEtat/
[lpmCConst]: /recours/lpm/CConst/
[renseignementCEtat]: /recours/renseignement/CEtat/
[secretdgseCEtat]: /recours/secretdgse/CEtat/
[verificationcnctrCnctr]: /recours/verificationcnctr/Cnctr/
[verificationcnctrCEtat]: /recours/verificationcnctr/CEtat/
---
body:


Suite aux révélations dans la presse concernant le système d'interception des communications transitant par les câbles sous-marins, les Exégètes ont attaqué le caractère illégal et inconstitutionnel de l'autorisation secrète de 2008… En vain, le Conseil d'État a rejeté considérant qu'il n'y avait pas suffisamment d'éléments de preuve pour contredire la parole du ministère de la défense.

### Explication des procédures


Deux procédures parallèles ont été initiées contre la surveillance
secrète de la DGSE dévoilée par un article de
[L'Obs](http://tempsreel.nouvelobs.com/societe/20150625.OBS1569/exclusif-comment-la-france-ecoute-aussi-le-monde.html)
de juillet 2015. L'article révèle la captation massive de
communications par les services de renseignement extérieur des flux
internet en provenance ou à destination du territoire français.


D'après cet article, ce système de surveillance de la DGSE aurait été autorisé par un « décret secret ».

La **première procédure** est une procédure d'urgence: un
référé-suspension au Conseil d'État. 

Cette procédure d'urgence avait pour but de demander la suspension
immédiate du décret en attendant le jugement sur le fond.

Ce recours a été rejeté dans
[une ordonnance du 9 septembre 2015](https://exegetes.eu.org/recours/secretdgse/CEtat/2015-09-09-Ordonnance_-_De__cret_secret_393079_ocr.pdf)
signée de la main de Bernard Stirn, président de la section du
contentieux du Conseil d'État, avant même la tenue d'une audience
contradictoire. Dans cette décision, le juge des référés estime que
l'urgence n'est pas caractérisée.

La **seconde procédure** a mené au rejet par le Conseil d'État sur le fond.

Les points attaqués portaient sur les éléments suivants :

  -  Légalité externe : Décret entaché d'incompétence et adopté au terme d'une procédure irrégulière ;
  -  Légalité interne : Décret dépourvu de base légale. La loi sur le renseignement adoptée en juillet 2015 aurait du apporter un fondement légal à ce décret, mais les dispositions relatives à la surveillance internationale ont été censurées par le Conseil constitutionnel.


*Procédure en collaboration avec Spinosi & Sureau SCP*

-------------

### Mémoires

- [31 août 2015 : Recours en référé suspension](2015-08-31-recours_dcret_secret_2008_rfr_suspension.pdf)
- [9 septembre 2015 : Ordonnance de rejet](2015-09-09-ordonnance_-_de__cret_secret_393079_ocr.pdf)
- [30 novembre 2015 : Mémoire complémentaire sur la procédure au fond](2015-11-30-quadrature-du-net-et-al.-decret-secret-2008-mc.pdf)
- [1er avril 2016 : Mémoire en réplique au gouvernement](2016-04-01-quadrature-du-net-et-autres-decret-secret-2008-replique.pdf)
- [7 octobre 2016 : Note en délibéré produite après l'audience](2016-10-07-quadrature-du-net-et-autres-d_cret-secret-note-en-d_lib_r_.doc)
- [18 novembre 2016 : Décision du Conseil d'État](2016-11-18-3930801pr1.pdf)

#### Recours initiés par d'autres 

 -   Un avocat franco-américain [saisit la CNCIS](https://www.laquadrature.net/fr/node/9538)
 -   La French American Bar Association [saisit la CNIL](http://www.nextinpact.com/news/96635-surveillance-internationale-avocats-franco-americains-portent-plainte-aupres-cnil.htm)



[abrogationretentiondemande]: /recours/abrogationretention/demande/
[abrogationretentionCEtat]: /recours/abrogationretention/CEtat/
[amicusrenseignement]: /recours/amicusrenseignement/
[filtragecazeneuveCEtat]: /recours/filtragecazeneuve/CEtat/
[filtragecazeneuveCnil]: /recours/filtragecazeneuve/Cnil/
[filtragecazeneuveOclctic]: /recours/filtragecazeneuve/Oclctic/
[lpmCEtat]: /recours/lpm/CEtat/
[lpmCConst]: /recours/lpm/CConst/
[renseignementCEtat]: /recours/renseignement/CEtat/
[secretdgseCEtat]: /recours/secretdgse/CEtat/
[verificationcnctrCnctr]: /recours/verificationcnctr/Cnctr/
[verificationcnctrCEtat]: /recours/verificationcnctr/CEtat/
---
title: Système de surveillance secret de la DGSE
