_model: page
---
title: Sophie in 't Veld c. DGSE
---
body:

<div class="bandeau"><span class="statut">Saisine de la Cour européenne des droits de l'homme (à Strasbourg)</span></div>

Les Exégètes ont assisté la député du Parlement européen Sophie in 't Veld contre la surveillance massive des communications internationales que la DGSE réalise depuis 2008.

### Explication des procédures

À la suite de l'adoption de la [loi sur la surveillance internationale][renseignement] entérinant le système de surveillance de la DGSE [mis en place depuis 2008][secretdgse], Sophie in 't Veld a utilisé les nouvelles dispositions du code de la sécurité intérieure pour envoyer une lettre de réclamation à la Commission nationale de contrôle des techniques de renseignement. Devant l'absence de réponse dans les délais de la Commission, Sophie in 't Veld a lancé deux procédures devant le Conseil d'État : l'une dans le cadre du **contentieux de la formation spéciale du Conseil d'État créé par la loi renseignement** ; l'autre dans le cadre d'un **recours pour excès de pouvoir de droit commun**.

Le Conseil d'État a mis la procédure en accéléré, justifiant d'une urgence… la décision n'est finalement intervenue que près de deux ans plus tard, en juin 2018. Le Conseil d'État (dans sa formation soumise au secret défense) rejette les recours justifiant que le droit français ne permet pas le recours au juge en matière de technique de renseignement sur des communications dites internationales.

Sophie in't Veld a annoncé son souhait de [saisir la Cour européenne des droits de l'homme](https://www.sophieintveld.eu/in-t-veld-challenges-french-surveillance-law-in-european-court-of-human-rights/).


[renseignement]: /dossiers/renseignement/index.html
[secretdgse]: /dossiers/secretdgse/index.html

### Mémoires et étapes de la procédure

 - [2 mai 2016 : Lettre de réclamation à la **Commission nationale de contrôle des techniques de renseignement**](2016-05-02-lettre-cnctr.pdf)
 - [30 juin 2016 : Seconde lettre](2016-06-30-letter-cnctr-30-juin-2016.pdf)
 - 5 octobre 2016 : décision du Conseil d'État de réduire le délai de production du mémoire ampliatif de 3 à 1 mois.
 - [13 novembre 2016 : Mémoire ampliatif au **Conseil d'État** dans la procédure de *vérification prévue par le code de la sécurité intérieure*](2016-11-13-ma-csi-principal.pdf)
 - [13 novembre 2016 : Mémoire ampliatif au **Conseil d'État** dans la procédure de *recours pour excès de pouvoir*](2016-11-13-ma-rep-principal.pdf)
 - 3 février 2016 : Réponses du gouvernement
 - fin février / début mars : Demande de délai supplémentaire pour répondre au gouvernement et constitution d'un avocat
 - [14 mars 2017 : Réplique pour contrer l'argumentation du gouvernement dans la procédure de *vérification CSI*](2017-03-14-replique-csi-premierministre.pdf)
 - [20 mars 2017 : Réplique pour contrer l'argumentation du gouvernement dans la procédure de *recours pour excès de pouvoir*](2017-03-20-replique-rep-premierministre.pdf)
 - novembre 2017 : réception de deux mémoires en défense du gouvernement (dit "dupliques").
 - [12 février 2018 : Mémoire d'observations sur la duplique dans la procédure de *recours pour excès de pouvoir*](2018-02-12-observations-sur-duplique-sans-coordonnees.pdf)
 - [20 juin 2018 : Décision du Conseil d'État - formation spécialisée du renseignement](https://www.legifrance.gouv.fr/affichJuriAdmin.do?oldAction=rechJuriAdmin&idTexte=CETATEXT000037089160&fastReqId=735654497&fastPos=1)


-------

[Article sur Médiapart](https://www.mediapart.fr/journal/international/281116/surveillance-une-elue-des-pays-bas-engage-un-bras-de-fer-avec-paris?onglet=full) (réservé aux abonnés)


<style type="text/css">
.bandeau {
height: 350px;
width: 100%;
background-image: url(submarine_1000.jpg);
background-size: cover;
margin-bottom: 1.4em;
}
.statut {
font-weight: bold;
color: #FFF;
background-color: #999;
opacity: 0.7;
}
</style>
